package com.foxminded.university.domain;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ScheduleItem {
	
	private Integer id;
	private Integer hour;
	private Integer subjectId;
	private Integer lecturerId;
	private Integer managerId;
	private LocalDate date;
}
