package com.foxminded.university.dto;

import lombok.Data;

@Data
public class ManagerDTO {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
}
