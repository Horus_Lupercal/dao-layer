package com.foxminded.university.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ScheduleItemDTO {
	
	private Integer id;
	private Integer hour;
	private Integer subjectId;
	private Integer lecturerId;
	private Integer managerId;
	private LocalDate date;
}
