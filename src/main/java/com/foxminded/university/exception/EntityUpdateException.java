package com.foxminded.university.exception;

public class EntityUpdateException extends RuntimeException {
	public EntityUpdateException(Class entityClass, int id) {
		super(String.format("Entity %s with id=%d, can't update", entityClass, id));
	}
}
