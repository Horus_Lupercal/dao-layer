package com.foxminded.university.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
	
	Optional<T> get(Integer id);
	
	List<T> getAll();
	
	Optional<T> save(T entity);
	
	Optional<T> update(T update);
	
	boolean delete(Integer id);
}
