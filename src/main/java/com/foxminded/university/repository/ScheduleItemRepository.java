package com.foxminded.university.repository;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleItemRepository<T> extends Repository<T> {
	
	List<T> showItemsByPeriod(LocalDate from, LocalDate to);
	
	int viewTotalLessonsHours();
	
	int viewLessonsHoursByPeriod(LocalDate from, LocalDate to);
}
