package com.foxminded.university.repository;

import java.util.List;

import com.foxminded.university.domain.Subject;

public interface SubjectRepository<T> extends Repository<T> {
	
	List<Subject> showStudentSubject(Integer studentId);
	
	int[] addSubjectToStudent(Integer studentId, List<Subject> subjects);
}
