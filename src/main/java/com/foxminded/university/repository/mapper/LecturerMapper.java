package com.foxminded.university.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.domain.enums.Rank;

public class LecturerMapper implements RowMapper<Lecturer> {
	
	@Override
	public Lecturer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Lecturer lecturer = new Lecturer();
		lecturer.setId(rs.getInt("id"));
		lecturer.setFirstName(rs.getString("first_name"));
		lecturer.setLastName(rs.getString("last_name"));
		lecturer.setRank(Rank.valueOf(rs.getString("rank")));
		lecturer.setAge(rs.getInt("age"));
		return lecturer;
	}
}
