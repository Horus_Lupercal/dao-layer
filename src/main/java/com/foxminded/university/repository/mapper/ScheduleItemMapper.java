package com.foxminded.university.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.foxminded.university.domain.ScheduleItem;

public class ScheduleItemMapper implements RowMapper<ScheduleItem> {
	
	@Override
	public ScheduleItem mapRow(ResultSet rs, int rowNum) throws SQLException {
		ScheduleItem item = new ScheduleItem();
		item.setId(rs.getInt("id"));
		item.setLecturerId(rs.getInt("lecturer_id"));
		item.setManagerId(rs.getInt("manager_id"));
		item.setSubjectId(rs.getInt("subject_id"));
		item.setHour(rs.getInt("hour"));
		item.setDate(rs.getDate("dt").toLocalDate());
		return item;
	}
}
