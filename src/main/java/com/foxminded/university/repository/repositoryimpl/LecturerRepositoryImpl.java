package com.foxminded.university.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.repository.Repository;
import com.foxminded.university.repository.mapper.LecturerMapper;

@Component
public class LecturerRepositoryImpl implements Repository<Lecturer> {
	
	private JdbcTemplate jdbcTemplate;
	private LecturerMapper mapper; 
	
	@Autowired
	public LecturerRepositoryImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		mapper = new LecturerMapper(); 
	}
	
	@Override
	public Optional<Lecturer> get(Integer id) {
		
		Lecturer lecturer = jdbcTemplate.queryForObject(
				"SELECT id, first_name, last_name, age, rank FROM lecturers WHERE id = ?;", mapper, id);
		return Optional.of(lecturer);
	}
	
	@Override
	public List<Lecturer> getAll() {
		return jdbcTemplate.query("SELECT id, first_name, last_name, age, rank FROM lecturers;", mapper);
	}
	
	@Override
	public Optional<Lecturer> save(Lecturer lecturer) {
		String sql = "INSERT INTO lecturers(id, first_name, last_name, age, rank) VALUES (nextval('seq_lecturers'), ?, ?, ?, ?);";
		KeyHolder keyHolder = new GeneratedKeyHolder();
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, new String[] {"id"});
    	            pst.setString(1, lecturer.getFirstName());
    	            pst.setString(2, lecturer.getLastName());
    	            pst.setInt(3, lecturer.getAge());
    	            pst.setString(4, lecturer.getRank().toString());
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	
    	Integer key = null;
    	if ((key = (Integer) keyHolder.getKey()) != null) {
    		lecturer.setId(key);
    		return Optional.of(lecturer);	
    	}
		return Optional.empty();
	}
	
	@Override
	public Optional<Lecturer> update(Lecturer update) {
		int count = jdbcTemplate.update(
				  "UPDATE lecturers SET first_name=?, last_name=?, age=?, rank=? WHERE id=?;", 
				update.getFirstName(), update.getLastName(), update.getAge(), update.getRank().toString(), update.getId());
		
		if (count > 0) {
			return Optional.of(update);
		}
		return Optional.empty();
	}
	
	@Override
	public boolean delete(Integer id) {
		int count = jdbcTemplate.update(
				"DELETE FROM lecturers WHERE id=?;", id);

		return count > 0;
	}
}
