package com.foxminded.university.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.Manager;
import com.foxminded.university.repository.Repository;

@Component
public class ManagerRepositoryImpl implements Repository<Manager> {
	
	private JdbcTemplate jdbcTemplate;
	private BeanPropertyRowMapper<Manager> mapper;
	
	@Autowired
	public ManagerRepositoryImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		mapper = new BeanPropertyRowMapper<>(Manager.class); 
	}
	
	@Override	
	public Optional<Manager> get(Integer id) {
		Manager manager = jdbcTemplate.queryForObject(
				"SELECT id, first_name, last_name, age FROM managers WHERE id = ?;", mapper, id);
		return Optional.of(manager);
	}
	
	@Override
	public List<Manager> getAll() {
		return jdbcTemplate.query("SELECT id, first_name, last_name, age FROM managers;", mapper);
	}
	
	@Override
	public Optional<Manager> save(Manager manager) {
		String sql = "INSERT INTO managers(id, first_name, last_name, age) VALUES (nextval('seq_managers'), ?, ?, ?);";
		KeyHolder keyHolder = new GeneratedKeyHolder();
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, new String[] {"id"});
    	            pst.setString(1, manager.getFirstName());
    	            pst.setString(2, manager.getLastName());
    	            pst.setInt(3, manager.getAge());
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	
    	Integer key = null;
    	if ((key = (Integer) keyHolder.getKey()) != null) {
    		manager.setId(key);
    		return Optional.of(manager);	
    	}
		return Optional.empty();
	}
	
	@Override
	public Optional<Manager> update(Manager update) {
		int count = jdbcTemplate.update(
				  "UPDATE managers SET first_name=?, last_name=?, age=? "
				+ "WHERE id=?;", 
				update.getFirstName(), update.getLastName(), update.getAge(), update.getId());
		
		if (count > 0) {
			return Optional.of(update);
		}
		return Optional.empty();
	}
	
	@Override
	public boolean delete(Integer id) {
		int count = jdbcTemplate.update(
				"DELETE FROM managers WHERE id=?;", id);
		return count > 0;
	}
}
