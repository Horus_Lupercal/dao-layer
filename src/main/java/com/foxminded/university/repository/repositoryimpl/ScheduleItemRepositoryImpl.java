package com.foxminded.university.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.ScheduleItem;
import com.foxminded.university.repository.ScheduleItemRepository;
import com.foxminded.university.repository.mapper.ScheduleItemMapper;

@Component
public class ScheduleItemRepositoryImpl implements ScheduleItemRepository<ScheduleItem> {

	private JdbcTemplate jdbcTemplate;
	private ScheduleItemMapper mapper;
	
	@Autowired
	public ScheduleItemRepositoryImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		mapper = new ScheduleItemMapper();
	}
	
	@Override
	public Optional<ScheduleItem> get(Integer id) {
		ScheduleItem scheduleItem = jdbcTemplate.queryForObject(
				"SELECT id, manager_id, subject_id, lecturer_id, hour, dt FROM sсhedule_items WHERE id = ?;", mapper, id);
		return Optional.of(scheduleItem);
	}
	
	@Override
	public List<ScheduleItem> getAll() {
		return jdbcTemplate.query("SELECT id, manager_id, subject_id, lecturer_id, hour, dt FROM sсhedule_items;", mapper);
	}
	
	@Override
	public Optional<ScheduleItem> save(ScheduleItem item) {
		int count = jdbcTemplate.update(
				  "INSERT INTO sсhedule_items(id, manager_id, subject_id, lecturer_id, hour, dt) VALUES (nextval('seq_sсhedule_items'), ?, ?, ?, ?, ?);", 
				  item.getManagerId(), item.getSubjectId(), item.getLecturerId(), item.getHour(), item.getDate());
		if (count > 0) {
			return Optional.of(item);
		}
		return Optional.empty();
	}
	
	@Override
	public Optional<ScheduleItem> update(ScheduleItem update) {
		String sql = "UPDATE sсhedule_items SET manager_id=?, subject_id=?, lecturer_id=?, hour=?, dt=? WHERE id=?;";
		KeyHolder keyHolder = new GeneratedKeyHolder();
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, new String[] {"id"});
    	            pst.setInt(1, update.getManagerId());
    	            pst.setInt(2, update.getSubjectId());
    	            pst.setInt(3, update.getLecturerId());
    	            pst.setInt(4, update.getHour());
    	            pst.setString(5, update.getDate().toString());
    	            pst.setInt(6, update.getId());
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	
    	Integer key = null;
    	if ((key = (Integer) keyHolder.getKey()) != null) {
    		update.setId(key);
    		return Optional.of(update);	
    	}
		return Optional.empty();
	}
	
	@Override
	public boolean delete(Integer id) {
		int count = jdbcTemplate.update(
				"DELETE FROM sсhedule_items WHERE id=?;", id);
		return count > 0;
	}
	
	@Override
	public List<ScheduleItem> showItemsByPeriod(LocalDate from, LocalDate to) {
		
		String equary =
				"SELECT id, manager_id, subject_id, lecturer_id, hour, dt FROM sсhedule_items WHERE dt >= ? AND dt <= ?;";
		
		return jdbcTemplate.query(equary, new Object[]{from.toString(), to.toString()}, mapper);
	}
	
	@Override
	public int viewTotalLessonsHours() {
		return jdbcTemplate.queryForObject("SELECT SUM(hour) FROM sсhedule_items;", Integer.class);
	}
	
	@Override
	public int viewLessonsHoursByPeriod(LocalDate from, LocalDate to) {
		String equary =
				"SELECT SUM(hour) FROM sсhedule_items WHERE dt >= ? AND dt <= ?;";
		
		return jdbcTemplate.queryForObject(equary, new Object[]{from.toString(), to.toString()}, Integer.class);
	}

}
