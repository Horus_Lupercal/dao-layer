package com.foxminded.university.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.Student;
import com.foxminded.university.repository.Repository;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class StudentRepositoryImpl implements Repository<Student> {
	
	private JdbcTemplate jdbcTemplate;
	private BeanPropertyRowMapper<Student> mapper; 
	
	@Autowired
	public StudentRepositoryImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		mapper = new BeanPropertyRowMapper<>(Student.class);
	}
	
	@Override
	public Optional<Student> get(Integer id) {
		
		Student student = jdbcTemplate.queryForObject(
				"SELECT id, first_name, last_name, age FROM students WHERE id = ?;", mapper, id);
		if (student == null) {
			return Optional.empty();
		}
		return log.traceExit(log.traceEntry(() -> id), Optional.of(student));
	}

	@Override
	public List<Student> getAll() {
		return log.traceExit(jdbcTemplate.query("SELECT id, first_name, last_name, age FROM students;", mapper));
	}

	@Override
	public Optional<Student> save(Student student) {
		String sql = "INSERT INTO students(id, first_name, last_name, age) VALUES (nextval('seq_students'), ?, ?, ?);";
    	KeyHolder keyHolder = new GeneratedKeyHolder();
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, new String[] {"id"});
    	            pst.setString(1, student.getFirstName());
    	            pst.setString(2, student.getLastName());
    	            pst.setInt(3, student.getAge());
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	
    	Integer key = null;
    	if ((key = (Integer) keyHolder.getKey()) != null) {
    		student.setId(key);
    		return Optional.of(student);	
    	}
    	return Optional.empty();
	}

	@Override
	public Optional<Student> update(Student update) {
		int count = jdbcTemplate.update(
				  "UPDATE students SET first_name=?, last_name=?, age=? "
				+ "WHERE id=?;", 
				update.getFirstName(), update.getLastName(), update.getAge(), update.getId());
		
		if (count > 0) {
			return Optional.of(update);
		}
		return Optional.empty();
	}

	@Override
	public boolean delete(Integer id) {
		int count = jdbcTemplate.update(
				"DELETE FROM students WHERE id=?;", id);
		return count > 0;
	}
}
