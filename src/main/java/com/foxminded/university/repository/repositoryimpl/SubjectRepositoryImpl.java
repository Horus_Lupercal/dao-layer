package com.foxminded.university.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.Subject;
import com.foxminded.university.repository.SubjectRepository;

@Component
public class SubjectRepositoryImpl implements SubjectRepository<Subject> {
	
	private JdbcTemplate jdbcTemplate;
	private BeanPropertyRowMapper<Subject> mapper; 
	
	@Autowired
	public SubjectRepositoryImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		mapper = new BeanPropertyRowMapper<>(Subject.class);
	}
	
	@Override
	public Optional<Subject> get(Integer id) {
		Subject subject = jdbcTemplate.queryForObject(
				"SELECT id, name, description FROM subjects WHERE id = ?;", mapper, id);
		return Optional.of(subject);
	}
	
	@Override
	public List<Subject> getAll() {
		return jdbcTemplate.query("SELECT id, name, description FROM subjects;", mapper);
	}
	
	@Override
	public Optional<Subject> save(Subject subject) {
		String sql = "INSERT INTO subjects(id, name, description) VALUES (nextval('seq_subjects'), ?, ?);";
		KeyHolder keyHolder = new GeneratedKeyHolder();
    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, new String[] {"id"});
    	            pst.setString(1, subject.getName());
    	            pst.setString(2, subject.getDescription());
    	            
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	
    	Integer key = null;
    	if ((key = (Integer) keyHolder.getKey()) != null) {
    		subject.setId(key);
    		return Optional.of(subject);	
    	}
    	return Optional.empty();
	}
	
	@Override
	public Optional<Subject> update(Subject update) {
		int count = jdbcTemplate.update(
				  "UPDATE subjects SET name=?, description=?"
				+ "WHERE id=?;", 
				update.getName(), update.getDescription(), update.getId());
		
		if (count > 0) {
			return Optional.of(update);
		}
		return Optional.empty();
	}
	
	@Override
	public boolean delete(Integer id) {
		int count = jdbcTemplate.update(
				"DELETE FROM subjects WHERE id=?;", id);
		return count > 0;
	}
	
	@Override
	public List<Subject> showStudentSubject(Integer studentId) {
		String quary ="SELECT subjects.id, subjects.name, subjects.description " + 
				"FROM ((subjects " + 
				"INNER JOIN students_subjects ON subjects.id = students_subjects.subject_id) " + 
				"INNER JOIN students ON students.id=students_subjects.student_id AND students.id=?);";
		return jdbcTemplate.query(quary, mapper, studentId);
	}

	@Override
	public int[] addSubjectToStudent(Integer studentId, List<Subject> subjects) {
		return jdbcTemplate.batchUpdate(
				"INSERT INTO students_subjects(student_id, subject_id) VALUES (?, ?);",
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setInt(1, studentId);
						ps.setInt(2, subjects.get(i).getId());
					}

					public int getBatchSize() {
						return subjects.size();
					}
				});
	}
}
