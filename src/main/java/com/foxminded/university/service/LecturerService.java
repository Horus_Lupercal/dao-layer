package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.dto.LecturerDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;
import com.foxminded.university.repository.repositoryimpl.LecturerRepositoryImpl;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LecturerService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private LecturerRepositoryImpl dao;
	
	public LecturerService(LecturerRepositoryImpl dao) {
		this.dao = dao;
	}
	
	public List<LecturerDTO> getAllLecturers() {
		return lecturersToLecturersDTO(dao.getAll());
	}

	public LecturerDTO createLecturer(LecturerDTO create) {
		
		Lecturer lecturer = modelMapper.map(create, Lecturer.class);
		Lecturer response = dao.save(lecturer).orElseThrow(() -> new EntitySaveException(Lecturer.class));

		return log.traceExit(
			    log.traceEntry(() -> create),
			    modelMapper.map(response, LecturerDTO.class));
	}

	public LecturerDTO updateLecturer(LecturerDTO update) {
		
		Lecturer lecturer = modelMapper.map(update, Lecturer.class);
		Lecturer response = dao.update(lecturer)
				.orElseThrow(() -> new EntityUpdateException(Lecturer.class, update.getId()));

		return log.traceExit(
			    log.traceEntry(() -> update),
			    modelMapper.map(response, LecturerDTO.class));
	}

	public LecturerDTO getLecturer(Integer id) {

		Lecturer response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Lecturer.class, id));
		
		return log.traceExit(
			    log.traceEntry(() -> id),
			    modelMapper.map(response, LecturerDTO.class));
	}

	public boolean deleteLecturer(Integer id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		return dao.delete(id);		
	}
	
	private List<LecturerDTO> lecturersToLecturersDTO(List<Lecturer> lecturers) {

		List<LecturerDTO> lecturersDto = lecturers.stream().map(source -> modelMapper.map(source, LecturerDTO.class))
				.collect(Collectors.toList());

		return log.traceExit(
				log.traceEntry(() -> lecturers), 
				lecturersDto);
	}
}
