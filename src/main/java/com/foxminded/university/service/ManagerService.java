package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.domain.Manager;
import com.foxminded.university.dto.ManagerDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;
import com.foxminded.university.repository.repositoryimpl.ManagerRepositoryImpl;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ManagerService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private ManagerRepositoryImpl dao;
	
	public ManagerService(ManagerRepositoryImpl dao) {
		this.dao = dao;
	}
	
	public List<ManagerDTO> getAllManagers() {
		
		List<ManagerDTO> managersDto = dao.getAll().stream().map(source -> modelMapper.map(source, ManagerDTO.class))
				.collect(Collectors.toList());

		return log.traceExit(managersDto);
	}

	public ManagerDTO createManager(ManagerDTO create) {
		
		Manager manager = modelMapper.map(create, Manager.class);
		Manager response = dao.save(manager).orElseThrow(() -> new EntitySaveException(Manager.class));

		return log.traceExit(
			    log.traceEntry(() -> create),
			    modelMapper.map(response, ManagerDTO.class));
	}

	public ManagerDTO updateManager(ManagerDTO update) {
		
		Manager manager = modelMapper.map(update, Manager.class);
		Manager response = dao.update(manager)
				.orElseThrow(() -> new EntityUpdateException(Manager.class, update.getId()));

		return log.traceExit(
			    log.traceEntry(() -> update),
			    modelMapper.map(response, ManagerDTO.class));
	}

	public ManagerDTO getManager(Integer id) {

		Manager response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Manager.class, id));
		
		return log.traceExit(
			    log.traceEntry(() -> id),
			    modelMapper.map(response, ManagerDTO.class));
	}

	public boolean deleteManager(Integer id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		return dao.delete(id);		
	}
}
