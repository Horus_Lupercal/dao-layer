package com.foxminded.university.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.domain.ScheduleItem;
import com.foxminded.university.dto.ScheduleItemDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;
import com.foxminded.university.repository.repositoryimpl.ScheduleItemRepositoryImpl;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ScheduleItemService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private ScheduleItemRepositoryImpl dao;
	
	public ScheduleItemService(ScheduleItemRepositoryImpl dao) {
		this.dao = dao;
	}
	
	public List<ScheduleItemDTO> getAllScheduleItems() {
		
		List<ScheduleItemDTO> scheduleItemsDto = dao.getAll().stream().map(source -> modelMapper.map(source, ScheduleItemDTO.class))
				.collect(Collectors.toList());

		return log.traceExit(scheduleItemsDto);
	}

	public ScheduleItemDTO createScheduleItem(ScheduleItemDTO create) {
		
		ScheduleItem scheduleItem = modelMapper.map(create, ScheduleItem.class);
		ScheduleItem response = dao.save(scheduleItem).orElseThrow(() -> new EntitySaveException(ScheduleItem.class));

		return log.traceExit(
			    log.traceEntry(() -> create),
			    modelMapper.map(response, ScheduleItemDTO.class));
	}

	public ScheduleItemDTO updateScheduleItem(ScheduleItemDTO update) {
		
		ScheduleItem scheduleItem = modelMapper.map(update, ScheduleItem.class);
		ScheduleItem response = dao.update(scheduleItem)
				.orElseThrow(() -> new EntityUpdateException(ScheduleItem.class, update.getId()));

		return log.traceExit(
			    log.traceEntry(() -> update),
			    modelMapper.map(response, ScheduleItemDTO.class));
	}

	public ScheduleItemDTO getScheduleItem(Integer id) {

		ScheduleItem response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(ScheduleItem.class, id));
		
		return log.traceExit(
			    log.traceEntry(() -> id),
			    modelMapper.map(response, ScheduleItemDTO.class));
	}

	public boolean deleteScheduleItem(Integer id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		return dao.delete(id);		
	}
	
	public List<ScheduleItemDTO> findItemsByPeriod(LocalDate from, LocalDate to) {
		
		List<ScheduleItemDTO> scheduleItemsDto = dao.showItemsByPeriod(from, to).stream().map(
				item -> modelMapper.map(item, ScheduleItemDTO.class)).collect(Collectors.toList());
		
		return log.traceExit(
			    log.traceEntry(() -> from.toString() + " " + to.toString()),
			    scheduleItemsDto);
	}
	
	public int viewTotalLessonsHours() {
		
		int hours = dao.viewTotalLessonsHours();
		
		return log.traceExit(hours);
	}
		
	public int viewLessonsHoursByPeriod(LocalDate from, LocalDate to) {
		
		int hours = dao.viewLessonsHoursByPeriod(from, to);
		
		return log.traceExit(log.traceEntry(() -> from.toString() + " " + to.toString()),
				hours);
	}
}
