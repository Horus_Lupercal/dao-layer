package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.domain.Student;
import com.foxminded.university.dto.StudentDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;
import com.foxminded.university.repository.repositoryimpl.StudentRepositoryImpl;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StudentService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private StudentRepositoryImpl dao;

	public StudentService(StudentRepositoryImpl dao) {
		this.dao = dao;
	}

	public List<StudentDTO> getAllStudent() {
		List<StudentDTO> students = dao.getAll().stream().map(source -> modelMapper.map(source, StudentDTO.class))
				.collect(Collectors.toList());
		
		return log.traceExit(students);
	}

	public StudentDTO createStudent(StudentDTO create) {
		
		Student student = modelMapper.map(create, Student.class);
		Student response = dao.save(student).orElseThrow(() -> new EntitySaveException(Student.class));

		return log.traceExit(
			    log.traceEntry(() -> create),
			    modelMapper.map(response, StudentDTO.class));
	}

	public StudentDTO updateStudent(StudentDTO update) {
		
		Student student = modelMapper.map(update, Student.class);
		Student response = dao.update(student)
				.orElseThrow(() -> new EntityUpdateException(Student.class, update.getId()));

		return log.traceExit(
			    log.traceEntry(() -> update),
			    modelMapper.map(response, StudentDTO.class));
	}

	public StudentDTO getStudent(Integer id) {

		Student response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Student.class, id));
		
		return log.traceExit(
			    log.traceEntry(() -> id),
			    modelMapper.map(response, StudentDTO.class));
	}

	public boolean deleteStudent(Integer id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		return dao.delete(id);		
	}
}
