package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.domain.Subject;
import com.foxminded.university.dto.SubjectDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;
import com.foxminded.university.repository.repositoryimpl.SubjectRepositoryImpl;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class SubjectService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private SubjectRepositoryImpl dao;
	
	public SubjectService(SubjectRepositoryImpl dao) {
		this.dao = dao;
	}
	
	public List<SubjectDTO> getAllSubjects() {
		
		List<SubjectDTO> subjectsDto = dao.getAll().stream().map(source -> modelMapper.map(source, SubjectDTO.class))
				.collect(Collectors.toList());

		return log.traceExit(subjectsDto);
	}

	public SubjectDTO createSubject(SubjectDTO create) {
		
		Subject subject = modelMapper.map(create, Subject.class);
		Subject response = dao.save(subject).orElseThrow(() -> new EntitySaveException(Subject.class));

		return log.traceExit(
			    log.traceEntry(() -> create),
			    modelMapper.map(response, SubjectDTO.class));
	}

	public SubjectDTO updateSubject(SubjectDTO update) {
		
		Subject subject = modelMapper.map(update, Subject.class);
		Subject response = dao.update(subject)
				.orElseThrow(() -> new EntityUpdateException(Subject.class, update.getId()));

		return log.traceExit(
			    log.traceEntry(() -> update),
			    modelMapper.map(response, SubjectDTO.class));
	}

	public SubjectDTO getSubject(Integer id) {

		Subject response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Subject.class, id));
		
		return log.traceExit(
			    log.traceEntry(() -> id),
			    modelMapper.map(response, SubjectDTO.class));
	}

	public boolean deleteSubject(Integer id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		return dao.delete(id);		
	}
	
	public List<SubjectDTO> getStudentSubejcts(Integer studentId) {
		
		List<SubjectDTO> subjects = dao.showStudentSubject(studentId).stream().map(source -> modelMapper.map(source, SubjectDTO.class))
				.collect(Collectors.toList());
		
		return log.traceExit(subjects);
	}
}
