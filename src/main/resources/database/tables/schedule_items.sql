DROP SEQUENCE IF EXISTS seq_sсhedule_items;

CREATE SEQUENCE IF NOT EXISTS seq_sсhedule_items
START WITH 1
INCREMENT BY 1;

DROP TABLE IF EXISTS sсhedule_items CASCADE;

CREATE TABLE sсhedule_items
( 
	id INT NOT NULL,
	dt DATE,
	hour INT not null,
	subject_id INT REFERENCES subjects(id) ON DELETE CASCADE,
	lecturer_id INT REFERENCES lecturers(id) ON DELETE CASCADE,
	manager_id INT REFERENCES managers(id) ON DELETE CASCADE,
	PRIMARY KEY (id, subject_id, lecturer_id, manager_id),
	UNIQUE (id, subject_id, lecturer_id, manager_id)
);

GRANT ALL ON SCHEMA public TO university_owner;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO university_owner;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO university_owner;