package com.foxminded.university.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.domain.enums.Rank;
import com.foxminded.university.repository.repositoryimpl.LecturerRepositoryImpl;
import com.foxminded.university.testconfiguration.TestConfiguration;

@ExtendWith(SpringExtension.class) 
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfiguration.class})
@Sql({"/lecturers.sql"})
public class LecturerRepositoryTest {
	
	@Autowired
	private LecturerRepositoryImpl dao;
	
	@Test
	public void testSaveLecturerfromDatabaseExceptedOk() {
		Lecturer lecturer = getLecturer();
		Lecturer responce = dao.save(lecturer).get();

		Assertions.assertThat(lecturer).isEqualToComparingFieldByField(responce);
	}
	
	@Test
	public void testGetLecturerFromDataBaseExceptedOk() {
		Lecturer lecturer = getLecturer();
		dao.save(lecturer).get();
		
		Lecturer response = dao.get(1).get();
		
		Assertions.assertThat(lecturer).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testGetAllLecturerAndExceptedIsOk() {
		Lecturer lecturer = getLecturer();
		dao.save(lecturer).get();
		
		List<Lecturer> response = dao.getAll();
		
		assertTrue(response.size() == 1);
		
		Assertions.assertThat(lecturer).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testPatchLecturerAndExceptedIsOk() {
		Lecturer lecturer = getLecturer();
		dao.save(lecturer).get();
		
		Lecturer update = new Lecturer(); 
		update.setId(1);
		update.setFirstName("update");
		update.setLastName("last");
		update.setAge(13);
		update.setRank(Rank.DOCENT);
		
		Lecturer response = dao.update(update).get();
		
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testDeleteStudenAndExceptedTrue() {
		Lecturer lecturer = getLecturer();
		dao.save(lecturer).get();
		
		assertTrue(dao.delete(1));
	}
	
	private Lecturer getLecturer() {
		Lecturer lecturer = new Lecturer();
		lecturer.setFirstName("Tolka");
		lecturer.setLastName("Ivam");
		lecturer.setRank(Rank.PROFESSOR);
		lecturer.setAge(10);
		return lecturer;
	}
}
