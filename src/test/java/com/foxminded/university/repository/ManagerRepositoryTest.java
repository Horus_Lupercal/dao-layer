package com.foxminded.university.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.foxminded.university.domain.Manager;
import com.foxminded.university.repository.repositoryimpl.ManagerRepositoryImpl;
import com.foxminded.university.testconfiguration.TestConfiguration;

@ExtendWith(SpringExtension.class) 
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfiguration.class})
@Sql({"/managers.sql"})
public class ManagerRepositoryTest {
	
	@Autowired
	private ManagerRepositoryImpl dao;
	
	@Test
	public void testSaveManagerfromDatabaseExceptedOk() {
		Manager manager = getManager();
		Manager responce = dao.save(manager).get();

		Assertions.assertThat(manager).isEqualToComparingFieldByField(responce);
	}
	
	@Test
	public void testGetManagerFromDataBaseExceptedOk() {
		Manager manager = getManager();
		dao.save(manager).get();
		
		Manager response = dao.get(1).get();
		
		Assertions.assertThat(manager).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testGetAllManagerAndExceptedIsOk() {
		Manager manager = getManager();
		dao.save(manager).get();
		
		List<Manager> response = dao.getAll();
		
		assertTrue(response.size() == 1);
		
		Assertions.assertThat(manager).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testPatchManagerAndExceptedIsOk() {
		Manager manager = getManager();
		dao.save(manager).get();
		
		Manager update = new Manager(); 
		update.setId(1);
		update.setFirstName("update");
		update.setLastName("last");
		update.setAge(13);
		
		Manager response = dao.update(update).get();
		
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testDeleteStudenAndExceptedTrue() {
		Manager manager = getManager();
		dao.save(manager).get();
		
		assertTrue(dao.delete(1));
	}
	
	private Manager getManager() {
		Manager manager = new Manager();
		manager.setFirstName("Tolka");
		manager.setLastName("Ivam");
		manager.setAge(10);
		return manager;
	}
}
