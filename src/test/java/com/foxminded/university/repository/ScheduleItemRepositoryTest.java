package com.foxminded.university.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.domain.Manager;
import com.foxminded.university.domain.ScheduleItem;
import com.foxminded.university.domain.Subject;
import com.foxminded.university.domain.enums.Rank;
import com.foxminded.university.repository.repositoryimpl.LecturerRepositoryImpl;
import com.foxminded.university.repository.repositoryimpl.ManagerRepositoryImpl;
import com.foxminded.university.repository.repositoryimpl.ScheduleItemRepositoryImpl;
import com.foxminded.university.repository.repositoryimpl.SubjectRepositoryImpl;
import com.foxminded.university.testconfiguration.TestConfiguration;

@ExtendWith(SpringExtension.class) 
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfiguration.class})
@Sql({"/subjects.sql", "/lecturers.sql", "/managers.sql", "/schedule_items.sql"})
public class ScheduleItemRepositoryTest {
	
	@Autowired
	private ScheduleItemRepositoryImpl itemDao;
	
	@Autowired
	private LecturerRepositoryImpl lecturerDao;
	
	@Autowired
	private ManagerRepositoryImpl managerDao;
	
	@Autowired
	private SubjectRepositoryImpl subjectDao;
	
	@Test
	public void testSaveScheduleItemfromDatabaseExceptedOk() {
		ScheduleItem item = getScheduleItem();
		ScheduleItem responce = itemDao.save(item).get();

		Assertions.assertThat(item).isEqualToComparingFieldByField(responce);
	}
	
	@Test
	public void testGetScheduleItemFromDataBaseExceptedOk() {
		ScheduleItem item = getScheduleItem();
		itemDao.save(item).get();
		
		ScheduleItem response = itemDao.get(1).get();
		
		Assertions.assertThat(item).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testGetAllScheduleItemAndExceptedIsOk() {
		ScheduleItem item = getScheduleItem();
		itemDao.save(item).get();
		
		List<ScheduleItem> response = itemDao.getAll();
		
		assertTrue(response.size() == 1);
		
		Assertions.assertThat(item).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testPatchScheduleItemAndExceptedIsOk() {
		ScheduleItem item = getScheduleItem();
		itemDao.save(item).get();
		
		ScheduleItem update = getScheduleItem(); 
		update.setId(1);
		update.setLecturerId(1);
		update.setManagerId(1);
		update.setSubjectId(1);
		update.setDate(LocalDate.now());
		update.setHour(4);
		
		ScheduleItem response = itemDao.update(update).get();
		
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testDeleteStudenAndExceptedTrue() {
		ScheduleItem item = getScheduleItem();
		itemDao.save(item).get();
		
		assertTrue(itemDao.delete(1));
	}
	
	@Test
	public void testFindItemByPeriod() {
		ScheduleItem item = getScheduleItem();
		LocalDate date = LocalDate.parse("2018-12-27");
		item.setDate(date);
		itemDao.save(item).get();
		
		LocalDate from = LocalDate.parse("2018-12-27");
		LocalDate to = LocalDate.parse("2020-12-27");
		
		List<ScheduleItem> response = itemDao.showItemsByPeriod(from, to);
		
		Assertions.assertThat(item).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testViewTotalLessonsHours() {
		ScheduleItem item = getScheduleItem();
		itemDao.save(item);
		
		int totalTime = itemDao.viewTotalLessonsHours();
		
		assertTrue(totalTime == item.getHour());
	}
	
	public void testViewTotalLessomsHoursByPeriod() {
		ScheduleItem item = getScheduleItem();
		LocalDate date = LocalDate.parse("2018-12-27");
		item.setDate(date);
		itemDao.save(item).get();
		
		LocalDate from = LocalDate.parse("2018-12-27");
		LocalDate to = LocalDate.parse("2020-12-27");
		
		int totalTime = itemDao.viewLessonsHoursByPeriod(from, to);
		
		assertTrue(totalTime == item.getHour());
	}
	
	private ScheduleItem getScheduleItem() {
		Subject subject = new Subject();
		subject.setDescription("description");
		subject.setName("name");
		subjectDao.save(subject);
		
		Manager manager = new Manager();
		manager.setFirstName("Tolka");
		manager.setLastName("Ivam");
		manager.setAge(10);
		managerDao.save(manager);
		
		Lecturer lecturer = new Lecturer();
		lecturer.setFirstName("Tolka");
		lecturer.setLastName("Ivam");
		lecturer.setRank(Rank.PROFESSOR);
		lecturer.setAge(10);
		lecturerDao.save(lecturer);
		
		ScheduleItem item = new ScheduleItem();
		item.setLecturerId(1);
		item.setManagerId(1);
		item.setSubjectId(1);
		item.setHour(2);
		item.setDate(LocalDate.now());
		return item;
	}
}
