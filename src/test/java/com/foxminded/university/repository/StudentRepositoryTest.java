package com.foxminded.university.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.foxminded.university.domain.Student;
import com.foxminded.university.repository.repositoryimpl.StudentRepositoryImpl;
import com.foxminded.university.testconfiguration.TestConfiguration;

@ExtendWith(SpringExtension.class) 
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfiguration.class})
@Sql({"/students.sql"})
public class StudentRepositoryTest {
	
	@Autowired
	private StudentRepositoryImpl dao;

	@Test
	public void testSaveStudentfromDatabaseExceptedOk() {
		Student student = getStudent();
		Student responce = dao.save(student).get();

		Assertions.assertThat(student).isEqualToComparingFieldByField(responce);
	}
	
	@Test
	public void testGetStudentFromDataBaseExceptedOk() {
		Student student = getStudent();
		dao.save(student).get();
		
		Student response = dao.get(1).get();
		
		Assertions.assertThat(student).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testGetAllStudentAndExceptedIsOk() {
		Student student = getStudent();
		dao.save(student).get();
		
		List<Student> response = dao.getAll();
		
		assertTrue(response.size() == 1);
		
		Assertions.assertThat(student).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testPatchStudentAndExceptedIsOk() {
		Student student = getStudent();
		dao.save(student).get();
		
		Student update = new Student(); 
		update.setId(1);
		update.setFirstName("update");
		update.setLastName("last");
		update.setAge(13);
		
		Student response = dao.update(update).get();
		
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testDeleteStudenAndExceptedTrue() {
		Student student = getStudent();
		student.setFirstName("Tolka");
		student.setLastName("Ivam");
		student.setAge(10);
		dao.save(student).get();
		
		assertTrue(dao.delete(1));
	}
	
	private Student getStudent() {
		Student student = new Student();
		student.setFirstName("Tolka");
		student.setLastName("Ivam");
		student.setAge(10);
		return student;
	}
}
