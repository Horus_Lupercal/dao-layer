package com.foxminded.university.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.foxminded.university.domain.Student;
import com.foxminded.university.domain.Subject;
import com.foxminded.university.repository.repositoryimpl.StudentRepositoryImpl;
import com.foxminded.university.repository.repositoryimpl.SubjectRepositoryImpl;
import com.foxminded.university.testconfiguration.TestConfiguration;

@ExtendWith(SpringExtension.class) 
@ActiveProfiles("test")
@ContextConfiguration(classes = {TestConfiguration.class})
@Sql({"/subjects.sql", "/students.sql", "/students_subjects.sql"})
public class SubjectRepositoryTest {
	
	@Autowired
	private SubjectRepositoryImpl subjectDao;
	
	@Autowired
	private StudentRepositoryImpl studentDao;

	@Test
	public void testSaveSubjectfromDatabaseExceptedOk() {
		Subject subject = getSubject();
		Subject responce = subjectDao.save(subject).get();

		Assertions.assertThat(subject).isEqualToComparingFieldByField(responce);
	}
	
	@Test
	public void testGetSubjectFromDataBaseExceptedOk() {
		Subject subject = getSubject();
		subjectDao.save(subject).get();
		
		Subject response = subjectDao.get(1).get();
		
		Assertions.assertThat(subject).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testGetAllSubjectAndExceptedIsOk() {
		Subject subject = getSubject();
		subjectDao.save(subject).get();
		
		List<Subject> response = subjectDao.getAll();
		
		assertTrue(response.size() == 1);
		
		Assertions.assertThat(subject).isEqualToIgnoringGivenFields(response.get(0), "id");
	}
	
	@Test
	public void testPatchSubjectAndExceptedIsOk() {
		Subject subject = getSubject();
		subjectDao.save(subject).get();
		
		Subject update = new Subject(); 
		update.setId(1);
		update.setDescription("description");
		update.setName("name");
		
		Subject response = subjectDao.update(update).get();
		
		Assertions.assertThat(update).isEqualToIgnoringGivenFields(response, "id");
	}
	
	@Test
	public void testDeleteStudenAndExceptedTrue() {
		Subject subject = getSubject();
		subjectDao.save(subject).get();
		
		assertTrue(subjectDao.delete(1));
	}
	
	@Test
	public void testAddSubjectToStudent() {
		Student student = new Student();
		student.setAge(19);
		student.setFirstName("Test");
		student.setLastName("testing");
		studentDao.save(student);
		
		Subject subject = getSubject();
		subjectDao.save(subject);
		subject.setId(1);
		
		List<Subject> subjects = new ArrayList<>();
		subjects.add(subject);
		
		int[] responce = subjectDao.addSubjectToStudent(1, subjects);
		
		assertTrue(responce[0] == 1);
	}
	
	@Test
	public void testShowAllStudentSubjects() {
		Student student = new Student();
		student.setAge(19);
		student.setFirstName("Test");
		student.setLastName("testing");
		studentDao.save(student);
		
		Subject subject = getSubject();
		subjectDao.save(subject);
		subject.setId(1);
		
		List<Subject> subjects = new ArrayList<>();
		subjects.add(subject);
		
		subjectDao.addSubjectToStudent(1, subjects);
		
		List<Subject> response = subjectDao.showStudentSubject(1);
		
		Assertions.assertThat(subject).isEqualToIgnoringGivenFields(response.get(0));
	}
	
	private Subject getSubject() {
		Subject subject = new Subject();
		subject.setDescription("description");
		subject.setName("name");
		return subject;
	}
}
