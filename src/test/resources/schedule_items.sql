DROP SEQUENCE IF EXISTS seq_sсhedule_items;

CREATE SEQUENCE IF NOT EXISTS seq_sсhedule_items
START WITH 1
INCREMENT BY 1;

DROP TABLE IF EXISTS sсhedule_items CASCADE;

CREATE TABLE sсhedule_items
( 
	id INT NOT NULL,
	hour INT not null,
	dt DATE,
	subject_id INT REFERENCES subjects(id) ON DELETE CASCADE,
	lecturer_id INT REFERENCES lecturers(id) ON DELETE CASCADE,
	manager_id INT REFERENCES managers(id) ON DELETE CASCADE,
	PRIMARY KEY (id, subject_id, lecturer_id, manager_id),
	UNIQUE (id, subject_id, lecturer_id, manager_id)
);