DROP TABLE IF EXISTS students_subjects CASCADE;

CREATE TABLE students_subjects
(
	student_id INT REFERENCES students(id) ON DELETE CASCADE,
	subject_id INT REFERENCES subjects(id) ON DELETE CASCADE,
	CONSTRAINT students_subject_pkey PRIMARY KEY (student_id, subject_id)
);
